# The name of the package:
atlas_subdir (TriggerAnalysis)

# Add the shared library:
atlas_add_library (TriggerAnalysisLib
  TriggerAnalysis/*.h Root/*.cxx
  PUBLIC_HEADERS TriggerAnalysis
  LINK_LIBRARIES AnaAlgorithmLib EventLoop xAODEventInfo xAODTrigger AsgAnalysisInterfaces TrigDecisionToolLib TrigConfInterfaces xAODHIEvent GoodRunsListsLib TrigConfxAODLib InDetTrackSelectionToolLib xAODJet JetCalibToolsLib JetSelectorToolsLib xAODTruth xAODEgamma ElectronPhotonSelectorToolsLib ElectronPhotonFourMomentumCorrectionLib MuonSelectorToolsLib MuonMomentumCorrectionsLib  PRIVATE_LINK_LIBRARIES)

if (XAOD_STANDALONE)
 # Add the dictionary (for AnalysisBase only):
 atlas_add_dictionary (TriggerAnalysisDict
  TriggerAnalysis/TriggerAnalysisDict.h
  TriggerAnalysis/selection.xml
  LINK_LIBRARIES TriggerAnalysisLib)
endif ()

if (NOT XAOD_STANDALONE)
  # Add a component library for AthAnalysis only:
  atlas_add_component (TriggerAnalysis
    src/components/*.cxx
    LINK_LIBRARIES TriggerAnalysisLib)
endif ()

# Install files from the package:
atlas_install_joboptions( share/*_jobOptions.py )
atlas_install_scripts( share/*_eljob.py )