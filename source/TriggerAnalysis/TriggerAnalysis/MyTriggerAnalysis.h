#ifndef TriggerAnalysis_MyTriggerAnalysis_H
#define TriggerAnalysis_MyTriggerAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
//HLT                                                                                                                                                                                  
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>

// ROOT                                                                                                                                                                                
#include <TH1.h>
#include <TTree.h>

// std includes                                                                                                                                                                        
#include <vector>
#include <string>
#include <map>

// E/gamma tools                                                                                                                                                                       
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"




class MyTriggerAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyTriggerAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  // output variables for the current event                                                                                                                                           
  /// \{                                                                                                               

  //trigger tools                                                                                                       
  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool;
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool;
                                                                
  unsigned int m_runNumber = 0; ///< Run number                                                                                                                      
  unsigned long long m_eventNumber = 0; ///< Event number                                                                                                           
  //FCal Info                                                                                                                                                                          
  float m_b_fcalA_et; //!                                                                                                                                           
  float m_b_fcalC_et; //!                                                                                                                                                              

  //HLT Triggers                                                                                                                                                   
  bool HLT_noalg_eb_L1TE50; //!                                                                                                                                     
  bool HLT_noalg_mb_L1TE50; //!

  bool HLT_noalg_eb_L1EM12; //!
  bool HLT_noalg_eb_L1EM14; //!
  bool HLT_noalg_eb_L1EM16; //!
  bool HLT_noalg_eb_L1EM22; //!


  bool HLT_noalg_cc_L1TE600_0ETA49; //!
  bool HLT_noalg_eb_L1TE12000; //!
  bool HLT_noalg_eb_L1ZDC_A_C_VTE50; //!                                                                                                                             
  bool HLT_noalg_eb_L1MU4; //!                                                                                                                                      
  bool HLT_g15_loose_ion; //!                                                                                                                                        
  bool HLT_g20_loose_ion; //!                                                                                                                                        
  bool HLT_g20_loose; //!                                                                                                                                            
  bool HLT_g30_loose_ion; //!                                                                                                                                       
  bool HLT_g13_etcut_ion; //!                                                                                                                                        
  bool HLT_g18_etcut_ion; //!                                                                                                                                       
  bool HLT_g18_etcut; //!                                                                                                                                            
  bool HLT_g28_etcut_ion; //!                                                                                                                                       
  bool HLT_g50_loose_ion; //!                                                                                                                                        
  bool HLT_noalg_L1EM10; //!                                                                                                                                        
  bool HLT_noalg_L1EM12; //!                                                                                                                                                           




  bool HLT_g15_loose_L1EM7; //!                                                                                                                                      
  bool HLT_g20_loose_L1EM15; //!                                                                                                                                     
  bool HLT_g25_loose_L1EM15; //!                                                                                                                                    
  bool HLT_g30_loose_L1EM15; //!                                                                                                                                    
  bool HLT_g35_loose_L1EM15; //!                                                                                                                                   

  //HLT Photons                                                                                                                                                   
  std::vector<bool> m_HLT_photon_loose_MC152; //! 

  std::vector<float> b_HLT_pt;//!                                                                                                                                
  std::vector<float> b_HLT_etaBE;//!                                                                                                                              
  std::vector<float> b_HLT_eta;//!                                                                                                                                
  std::vector<float> b_HLT_phi;//!                                                                                                                                

  int b_HLT_photons_n=0;//!                                                                                                                                         

  //L1 Photons Info                                                                                                                                                
  int b_L1_n = 0; //!                                                                                                                                               
  int count_me; //!                                                                                                                                                                    
  std::vector<float> b_L1_eta; //!                                                                                                                                  
  std::vector<float> b_L1_phi; //!                                                                                                                              
  std::vector<float> b_L1_pt; //!                                                                                                                                    
  std::vector<bool> b_L1_EM3; //!                                                                                                                                    
  std::vector<bool> b_L1_EM5; //!                                                                                     
  std::vector<bool> b_L1_EM10; //!                                                                                                                                   
  std::vector<bool> b_L1_EM12; //!                                                                                                                              
  std::vector<bool> b_L1_EM16; //!                                                                                                                                   
  std::vector<bool> b_L1_EM20; //! 

  // Photon Info                                                                                                                                                 
  int m_b_photon_n=0; //!                                                                                                                                          
  std::vector<float> m_b_photon_pt; //!                                                                                                                             
  std::vector<float> m_b_photon_eta; //!                                                                                                                           
  std::vector<float> m_b_photon_phi; //!                                                                                                                            
  std::vector<bool> m_b_photon_tight; //!                                                                                                                            
  std::vector<bool> m_b_photon_medium; //!                                                                                                                          
  std::vector<bool> m_b_photon_loose; //!                                                                                                                         
  std::vector<unsigned int> m_b_photon_isem; //!                                                                                                                    
  std::vector<int> m_b_photon_convFlag; //!                                                                                                                         
  std::vector<float> m_b_photon_Rconv; //!                                                                                                                           
  std::vector<float> m_b_photon_etcone20; //!                                                                                                                       
  std::vector<float> m_b_photon_etcone30; //!                                                                                                                        
  std::vector<float> m_b_photon_etcone40; //!                                                                                                                       
  std::vector<float> m_b_photon_topoetcone20; //!                                                                                                                   
  std::vector<float> m_b_photon_topoetcone30; //!                                                                                                                   
  std::vector<float> m_b_photon_topoetcone40; //!                                                                                                                   
  std::vector<float> m_b_photon_Rhad1; //!                                                                                                                         
  std::vector<float> m_b_photon_Rhad; //!                                                                                                                            
  std::vector<float> m_b_photon_e277; //!                                                                                                                           
  std::vector<float> m_b_photon_Reta; //!                                                                                                                            
  std::vector<float> m_b_photon_Rphi; //!                                                                                                                            
  std::vector<float> m_b_photon_weta1; //!                                                                                                                           
  std::vector<float> m_b_photon_weta2; //!                                                                                                                           
  std::vector<float> m_b_photon_wtots1; //!                            
  std::vector<float> m_b_photon_f1; //!                                                                                                                            
  std::vector<float> m_b_photon_f3; //!                                                                                                                              
  std::vector<float> m_b_photon_fracs1; //!                                                                                                                          
  std::vector<float> m_b_photon_DeltaE; //!                                                                                                                          
  std::vector<float> m_b_photon_Eratio; //! 
  
  // Egamma tools                                                                                                       
  AsgPhotonIsEMSelector* m_photonTightIsEMSelector; //!                                                                 
  AsgPhotonIsEMSelector* m_photonMediumIsEMSelector; //!                                                                
  AsgPhotonIsEMSelector* m_photonLooseIsEMSelector; //!                                                                 
  
  AsgPhotonIsEMSelector* m_photonOnlineLooseIsEMSelectorMC152; //!

  CP::EgammaCalibrationAndSmearingTool* m_egammaPtEtaPhiECorrector; //!  

};

#endif
