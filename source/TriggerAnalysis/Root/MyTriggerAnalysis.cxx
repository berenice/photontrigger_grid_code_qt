#include <AsgTools/MessageCheck.h>
#include <TriggerAnalysis/MyTriggerAnalysis.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

//ROOT                                                                                                                                                                                 
#include <TFile.h>
#include <TSystem.h>

// HI event includes                                                                                                                                                                   
#include "xAODHIEvent/HIEventShapeContainer.h"

//Cross Calibration                                                                                                                                                                    
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"

// Egamma & muons                                                                                                                                                                      
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

//#include "/afs/cern.ch/user/b/berenice/ROOTAnalysisTutorial/source/MyAnalysis/MyAnalysis/EmTauRoIContainer.h"                                                                        
#include "TriggerAnalysis/EmTauRoIContainer.h"

MyTriggerAnalysis :: MyTriggerAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
  m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool"),
  m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  m_photonTightIsEMSelector = nullptr;
  m_photonMediumIsEMSelector = nullptr;
  m_photonLooseIsEMSelector = nullptr;
  m_photonOnlineLooseIsEMSelectorMC152 = nullptr;
}



StatusCode MyTriggerAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
  count_me = 0;

  ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]                                                                                                         

  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));

  //Tree Created                                                                                                                                                                       
  TTree* mytree = tree ("analysis");

  //Run & Event Number                                                                                                                                                                 
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);



  //FCal                                                                                                                                                                               
  mytree->Branch ("fcalA_et", &m_b_fcalA_et, "fcalA_et/F");
  mytree->Branch ("fcalC_et", &m_b_fcalC_et, "fcalC_et/F");

  //------Triggers for Pb+Pb----------//                                                                                                                                               
  //HLT                                                                                                                                                                                
  mytree->Branch("HLT_noalg_eb_L1TE50",               &HLT_noalg_eb_L1TE50, "HLT_noalg_eb_L1TE50/O");
  mytree->Branch("HLT_noalg_eb_L1EM12",               &HLT_noalg_eb_L1EM12, "HLT_noalg_eb_L1EM12/O");
  mytree->Branch("HLT_noalg_eb_L1EM14",               &HLT_noalg_eb_L1EM14, "HLT_noalg_eb_L1EM14/O");
  mytree->Branch("HLT_noalg_eb_L1EM16",               &HLT_noalg_eb_L1EM16, "HLT_noalg_eb_L1EM14/O");
  mytree->Branch("HLT_noalg_eb_L1EM22",               &HLT_noalg_eb_L1EM22, "HLT_noalg_eb_L1EM22/O");

  mytree->Branch("HLT_noalg_eb_L1TE1200",             &HLT_noalg_eb_L1TE12000, "HLT_noalg_eb_L1TE12000/O");
  mytree->Branch("HLT_noalg_mb_L1TE50",               &HLT_noalg_mb_L1TE50, "HLT_noalg_mb_L1TE50/O");
  mytree->Branch("HLT_noalg_cc_L1TE600_0ETA49",       &HLT_noalg_cc_L1TE600_0ETA49, "HLT_noalg_cc_L1TE600_0ETA49/O");
  mytree->Branch("HLT_noalg_eb_L1ZDC_A_C_VTE50",      &HLT_noalg_eb_L1ZDC_A_C_VTE50, "HLT_noalg_eb_L1ZDC_A_C_VTE50/O");
  mytree->Branch("HLT_noalg_eb_L1MU4",                &HLT_noalg_eb_L1MU4, "HLT_noalg_eb_L1MU4/O");



  mytree->Branch("HLT_g30_loose_ion",                 &HLT_g30_loose_ion, "HLT_g30_loose_ion/O");
  mytree->Branch("HLT_g50_loose_ion",                 &HLT_g50_loose_ion, "HLT_g50_loose_ion/O");
  mytree->Branch("HLT_g20_loose",                     &HLT_g20_loose, "HLT_g20_loose/O");
  mytree->Branch("HLT_g20_loose_ion",                 &HLT_g20_loose_ion, "HLT_g20_loose_ion/O");
  mytree->Branch("HLT_g15_loose_ion",                 &HLT_g15_loose_ion, "HLT_g15_loose_ion/O");

  mytree->Branch("HLT_g13_etcut_ion",                 &HLT_g13_etcut_ion, "HLT_g13_etcut_ion/O");
  mytree->Branch("HLT_g18_etcut_ion",                 &HLT_g18_etcut_ion, "HLT_g18_etcut_ion/O");
  mytree->Branch("HLT_g18_etcut",                     &HLT_g18_etcut, "HLT_g18_etcut/O");

  mytree->Branch("HLT_g28_etcut_ion",                 &HLT_g28_etcut_ion, "HLT_g28_etcut_ion/O");
  mytree->Branch("HLT_noalg_L1EM10",                  &HLT_noalg_L1EM10, "HLT_noalg_L1EM10/O");
  mytree->Branch("HLT_noalg_L1EM12",                  &HLT_noalg_L1EM12, "HLT_noalg_L1EM12/O ");

  //HLT Online Loose  Photons 
  mytree->Branch("m_HLT_photon_loose_MC152", &m_HLT_photon_loose_MC152);


  //HLT Kinematics
  mytree->Branch("b_HLT_pt",                            &b_HLT_pt);
  mytree->Branch("b_HLT_etaBE",                         &b_HLT_etaBE);
  mytree->Branch("b_HLT_eta",                           &b_HLT_eta);
  mytree->Branch("b_HLT_phi",                           &b_HLT_phi);
  mytree->Branch("b_HLT_photons_n",                     &b_HLT_photons_n, "b_HLT_photons_n/I");


  //L1RO                                                                                                                                                                               
  mytree->Branch("b_L1_n",               &b_L1_n);
  mytree->Branch ("L1RO_eta",            &b_L1_eta);
  mytree->Branch ("L1RO_phi",            &b_L1_phi);
  mytree->Branch("L1RO_pt",              &b_L1_pt);

  mytree->Branch ("L1RO_EM3",            &b_L1_EM3);
  mytree->Branch ("L1RO_EM5",            &b_L1_EM5);
  mytree->Branch("L1RO_EM10",              &b_L1_EM10);
  mytree->Branch ("L1RO_EM12",            &b_L1_EM12);
  mytree->Branch ("L1RO_EM16",            &b_L1_EM16);
  mytree->Branch("L1RO_EM20",              &b_L1_EM20);




  // Photons                                                                                                                                             
  mytree->Branch ("photon_n",             &m_b_photon_n, "photon_n/I");
  mytree->Branch ("photon_pt",            &m_b_photon_pt);
  mytree->Branch ("photon_phi",           &m_b_photon_phi);
  mytree->Branch("photon_eta",            &m_b_photon_eta);
  mytree->Branch ("photon_tight",         &m_b_photon_tight);
  mytree->Branch ("photon_medium",        &m_b_photon_medium);
  mytree->Branch ("photon_loose",         &m_b_photon_loose);
  mytree->Branch ("photon_convFlag",      &m_b_photon_convFlag);
  mytree->Branch ("photon_Rconv",         &m_b_photon_Rconv);
  mytree->Branch ("photon_etcone20",      &m_b_photon_etcone20);
  mytree->Branch ("photon_etcone30",      &m_b_photon_etcone30);
  mytree->Branch ("photon_etcone40",      &m_b_photon_etcone40);
  mytree->Branch ("photon_topoetcone20",  &m_b_photon_topoetcone20);
  mytree->Branch ("photon_topoetcone30",  &m_b_photon_topoetcone30);
  mytree->Branch ("photon_topoetcone40",  &m_b_photon_topoetcone40);

  mytree->Branch ("photon_Rhad1",   &m_b_photon_Rhad1);
  mytree->Branch ("photon_Rhad",    &m_b_photon_Rhad);
  mytree->Branch ("photon_e277",    &m_b_photon_e277);
  mytree->Branch ("photon_Reta",    &m_b_photon_Reta);
  mytree->Branch ("photon_Rphi",    &m_b_photon_Rphi);
  mytree->Branch ("photon_weta1",   &m_b_photon_weta1);
  mytree->Branch ("photon_weta2",   &m_b_photon_weta2);
  mytree->Branch ("photon_wtots1",  &m_b_photon_wtots1);
  mytree->Branch ("photon_f1",      &m_b_photon_f1);
  mytree->Branch ("photon_f3",      &m_b_photon_f3);
  mytree->Branch ("photon_fracs1",  &m_b_photon_fracs1);
  mytree->Branch ("photon_DeltaE",  &m_b_photon_DeltaE);
  mytree->Branch ("photon_Eratio",  &m_b_photon_Eratio);



  // Initialize and configure trigger tools                                                                       
  ANA_CHECK (m_trigConfigTool.initialize());
  ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool                                         
  ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
 ANA_CHECK (m_trigDecisionTool.initialize());


 // Initialize photon tight selection tool                                                                             
 m_photonTightIsEMSelector = new AsgPhotonIsEMSelector  ("PhotonTightIsEMSelector");
 ANA_CHECK (m_photonTightIsEMSelector->setProperty ("isEMMask", egammaPID::PhotonTight));
 ANA_CHECK (m_photonTightIsEMSelector->setProperty ("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/Ph\
otonIsEMTightSelectorCutDefs.conf"));
 ANA_CHECK (m_photonTightIsEMSelector->initialize ());

 // Initialize photon medium selection tool                                                                            
 m_photonMediumIsEMSelector = new AsgPhotonIsEMSelector  ("PhotonMediumIsEMSelector");
 ANA_CHECK (m_photonMediumIsEMSelector->setProperty ("isEMMask", egammaPID::PhotonMedium));
 ANA_CHECK (m_photonMediumIsEMSelector->setProperty ("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/P\
hotonIsEMMediumSelectorCutDefs.conf"));
 ANA_CHECK (m_photonMediumIsEMSelector->initialize ());

 // Initialize photon loose selection tool                                                                             
 m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector  ("PhotonLooseIsEMSelector");
 ANA_CHECK (m_photonLooseIsEMSelector->setProperty ("isEMMask", egammaPID::PhotonLoose));
 ANA_CHECK (m_photonLooseIsEMSelector->setProperty ("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/Ph\
otonIsEMLooseSelectorCutDefs.conf"));
 ANA_CHECK (m_photonLooseIsEMSelector->initialize ());


 //Initialize Online Loose Selection Tool
 m_photonOnlineLooseIsEMSelectorMC152 = new AsgPhotonIsEMSelector ("PhotonLooseIsEMSelector");
 ANA_CHECK (m_photonOnlineLooseIsEMSelectorMC152->setProperty ("isEMMask", egammaPID::PhotonLoose));
 ANA_CHECK(m_photonOnlineLooseIsEMSelectorMC152->setProperty("ConfigFile","ElectronPhotonSelectorTools/trigger/rel21_20180312/PhotonIsEMLooseSelectorCutDefs.conf"));
 ANA_CHECK(m_photonOnlineLooseIsEMSelectorMC152->initialize());

  return StatusCode::SUCCESS;
}



StatusCode MyTriggerAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go  const xAOD::EventInfo *eventInfo = nullptr;
  
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  const xAOD::PhotonContainer* photons = 0;
  if (!evtStore ()->retrieve (photons, "Photons").isSuccess ())  {
    Error ("GetPhotons ()", "Failed to retrieve Photons collection. Exiting.");



    return EL::StatusCode::FAILURE;
  }



  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();


  //HLT                                                                                                                                                                                
  HLT_noalg_eb_L1TE50 =false;
  HLT_noalg_eb_L1EM12=false;
  HLT_noalg_eb_L1EM14=false;
  HLT_noalg_eb_L1EM22=false;
  HLT_noalg_eb_L1EM16=false;

  HLT_noalg_mb_L1TE50 = false;
  HLT_noalg_cc_L1TE600_0ETA49 = false;
  HLT_noalg_eb_L1TE12000 = false;
  HLT_noalg_eb_L1ZDC_A_C_VTE50 = false;
  HLT_noalg_eb_L1MU4 = false;


  HLT_g15_loose_ion = false;
  HLT_g20_loose_ion = false;
  HLT_g20_loose = false;
  HLT_g30_loose_ion = false;
  HLT_g13_etcut_ion = false;
  HLT_g18_etcut_ion = false;
  HLT_g18_etcut = false;
  HLT_g28_etcut_ion = false;
  HLT_g50_loose_ion = false;

  HLT_noalg_L1EM10 = false;
  HLT_noalg_L1EM12 = false;

  HLT_g15_loose_L1EM7 = false;
  HLT_g20_loose_L1EM15 = false;
  HLT_g25_loose_L1EM15 = false;
  HLT_g30_loose_L1EM15 = false;
  HLT_g35_loose_L1EM15 = false;

  //HLT Online Photons
  m_HLT_photon_loose_MC152.clear();

  //HLT Kinematics                                                                                                                                                                     
  b_HLT_pt.clear();
  b_HLT_phi.clear();
  b_HLT_eta.clear();
  b_HLT_etaBE.clear();
  b_HLT_photons_n = 0;

  //L1RO Kinematics                                                                                                                                                                    
  b_L1_eta.clear();
  b_L1_phi.clear();
  b_L1_pt.clear();

  //L1RO EMX                                                                                                                                                                           
  b_L1_EM3.clear();
  b_L1_EM5.clear();
  b_L1_EM10.clear();
  b_L1_EM12.clear();
  b_L1_EM16.clear();




  //Clear Photon Kinematic Vectors                                                                                                                                                        
  m_b_photon_pt.clear();
  m_b_photon_phi.clear();
  m_b_photon_eta.clear();

  //Clear Tight, Medium, and Loose Photon Vectors                                                                                                                                         
  m_b_photon_tight.clear();
  m_b_photon_medium.clear();
  m_b_photon_loose.clear();
  m_b_photon_isem.clear();
  m_b_photon_convFlag.clear();
  m_b_photon_Rconv.clear();

  //Clear Photon Cone Vectors                                                                                                                                                             
  m_b_photon_etcone20.clear();
  m_b_photon_etcone30.clear();
  m_b_photon_etcone40.clear();
  m_b_photon_topoetcone20.clear();
  m_b_photon_topoetcone30.clear();
  m_b_photon_topoetcone40.clear();

  //Clear Shower Shapes Vector                                                                                                                                                            
  m_b_photon_Rhad1.clear();
  m_b_photon_Rhad.clear();
  m_b_photon_e277.clear ();
  m_b_photon_Reta.clear ();
  m_b_photon_Rphi.clear ();
  m_b_photon_weta1.clear ();
  m_b_photon_weta2.clear ();
  m_b_photon_wtots1.clear ();
  m_b_photon_f1.clear ();
  m_b_photon_f3.clear ();
  m_b_photon_fracs1.clear ();
  m_b_photon_DeltaE.clear ();
  m_b_photon_Eratio.clear ();

  m_b_photon_n = 0;

  m_b_fcalA_et = 0;
  m_b_fcalC_et = 0;


  //--------------------------------------------------------------//                                                                                                                   
  //------------------------HLT Photons---------------------------//                                                                                                                   
  //--------------------------------------------------------------//                                                                                                                   


  const xAOD::PhotonContainer *HLT_photons =0;
  if(!evtStore()->retrieve( HLT_photons, "HLT_xAOD__PhotonContainer_egamma_Photons" ).isSuccess() ){
    std::cout << " failed to retrieve HLT_xAOD__PhotonContainer_egamma_Photons!" << std::endl;
    return EL::StatusCode::FAILURE;
  }

  for (const auto *hlt_photon : *HLT_photons) {
    if (hlt_photon->pt () * 1e-3 < 15) {continue;}


    //Online Loose Photon 
    m_HLT_photon_loose_MC152.push_back(m_photonOnlineLooseIsEMSelectorMC152->accept(hlt_photon));
    // Kinematics                                                                                                                                                                      
    b_HLT_pt.push_back (hlt_photon->pt () * 1e-3);
    b_HLT_phi.push_back (hlt_photon->phi ());
    b_HLT_etaBE.push_back(hlt_photon->caloCluster ()->etaBE (2));
    b_HLT_eta.push_back(hlt_photon->eta());
    b_HLT_photons_n++;

  }

  //--------------------------------------------------------------//                                                                                                                   
  //-----------------------Level 1 RO Photons---------------------//                                                                                                                   
  //--------------------------------------------------------------//                                                                                                                   

  const xAOD::EmTauRoIContainer *L1ROI = 0;
  if ( !evtStore()->retrieve( L1ROI, "LVL1EmTauRoIs" ).isSuccess() ){
    std::cout << " failed to retrieve LVL1EmTauRoIs!" << std::endl;
    return EL::StatusCode::FAILURE;
  }

  //if ( L1ROI->size() > 0 ) std::cout << " event has " << L1ROI->size() << " L1 ROIs" << std::endl;                                                                                   

  xAOD::EmTauRoIContainer::const_iterator it_L1 = L1ROI->begin();
  xAOD::EmTauRoIContainer::const_iterator it_e_L1 = L1ROI->end();




  int i_roi = 0;
  b_L1_n = 0;


  for ( ; it_L1 != it_e_L1; it_L1++)  {
    if( ((*it_L1)->roiWord() & 0xf0000000) != 0xa0000000 ) continue;
    b_L1_eta.push_back((*it_L1)->eta());
    b_L1_phi.push_back((*it_L1)->phi());
    b_L1_pt.push_back((*it_L1)->eT() * 1e-3);




    auto names = (*it_L1)->thrNames();
    //    if(first && (count_me==336)){                                                                                                                                                

    //std::cout << "This is the size of your vector names: " << names.size() << std::endl;                                                                                             
    //for (unsigned int nameTrig = 0; nameTrig < names.size(); nameTrig++){                                                                                                            

    //std::cout << "Trigger Name: " << names.at(nameTrig) << std::endl;                                                                                                                
    //std::cout << "Eta of L1RO: " << (*it_L1)->eta() << std::endl;                                                                                                                    
    //std::cout << "Phi of L1RO: " << (*it_L1)->phi() << std::endl;                                                                                                                    
    //std::cout << "Pt of L1RO: " << ((*it_L1)->eT() * 1e-3) << std::endl;                                                                                                             

    //}                                                                                                                                                                              
    //std::cout << "This is the value of your count_me: " << count_me << std::endl;                                                                                                    
    //first = 0;                                                                                                                                                                       

    //}                                                                                                                                                                                
    for (unsigned int i = 0; i < names.size(); i++) {

      //std::cout << "This is the name: " << names.at(i) << std::endl;                                                                                                                 

      if ( names.at(i) == "EM3" ){ b_L1_EM3.push_back(true);}else{ b_L1_EM3.push_back(false);}
      if ( names.at(i) == "EM5" ){ b_L1_EM5.push_back(true);}else{ b_L1_EM5.push_back(false);}
      if ( names.at(i) == "EM10" ){b_L1_EM10.push_back(true);}else{b_L1_EM10.push_back(false);}
      if ( names.at(i) == "EM12" ){b_L1_EM12.push_back(true);}else{b_L1_EM12.push_back(false);}
      if ( names.at(i) == "EM16" ){b_L1_EM16.push_back(true);}else{b_L1_EM16.push_back(false);}
      if ( names.at(i) == "EM20" ){b_L1_EM20.push_back(true);}else{b_L1_EM20.push_back(false);}
      //std::cout << "--> --> name #" << i << " --> " << names.at(i) << std::endl;                                                                                                     
    }

    /**                                                                                                                                                                                
    if (((*it_L1)->eT() * 1e-3) > 100){                                                                                                                                                
                                                                                                                                                                                       
    std::cout << "pt is greater than 100 GeV" << std::endl;                                                                                                                          
    std::cout << "pt: " << b_L1_pt.at(b_L1_n) << std::endl;                                                                                                                          
    std::cout << "--> ROI #" << i_roi << " , L1 eta/phi/ET = " << (*it_L1)->eta() << " / " << (*it_L1)->phi() << " / " << (*it_L1)->eT() * 1e-3 << ", word = " << std::hex << (*it_L\
    1)->roiWord() << ", EM3 / 5 / 10 / 12 / 16 / 20 = " <<  b_L1_EM3.at(b_L1_n) << " / " << b_L1_EM5.at(b_L1_n) << " / " << b_L1_EM10.at(b_L1_n) << " / " << b_L1_EM12.at(b_L1_n) << "" <<\
    b_L1_EM16.at(b_L1_n) << " / " << b_L1_EM20.at(b_L1_n) << std::endl;                                                                                                                   
    }                                                                                                                                                                                  
    */
    b_L1_n++;
    i_roi++;

  }


  const xAOD::HIEventShapeContainer* hiueContainer = 0;
  if (!evtStore ()->retrieve (hiueContainer, "HIEventShape").isSuccess ()) {
    Error ("GetHIEventShape ()", "Failed to retrieve HIEventShape container. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  for (const auto* hiue : *hiueContainer) {
    double et = hiue->et ();
    int layer = hiue->layer ();
    double eta = hiue->etaMin ();

    if (layer == 21 || layer == 22 || layer == 23) {
      if (eta > 0)  {
        m_b_fcalA_et += et * 1e-3;
      }
      else {
        m_b_fcalC_et += et * 1e-3;
      }
    }
  }



  for (const auto *photon : *photons) {


    //bool check_loose = m_photonLooseIsEMSelector->accept (photon);                                                                                                                   
    //bool check_pt = (photon->pt () * 1e-3 > 50);                                                                                                                                     

    //if(check_pt){record_event = true;}                                                                                                                                               
    if (photon->pt () * 1e-3 < 5) {continue;}

    // Kinematics                                                                                                                                                                      
    m_b_photon_pt.push_back (photon->pt () * 1e-3);
    m_b_photon_phi.push_back (photon->phi ());
    m_b_photon_eta.push_back(photon->caloCluster ()->etaBE (2));

    // Identification                                                                                                                                                                  
    m_b_photon_tight.push_back (m_photonTightIsEMSelector->accept (photon));
    m_b_photon_medium.push_back (m_photonMediumIsEMSelector->accept (photon));
    m_b_photon_loose.push_back (m_photonLooseIsEMSelector->accept (photon));
    m_b_photon_isem.push_back (m_photonTightIsEMSelector->IsemValue ());
    m_b_photon_convFlag.push_back (xAOD::EgammaHelpers::conversionType (photon));
    m_b_photon_Rconv.push_back (xAOD::EgammaHelpers::conversionRadius (photon));

    // Isolation                                                                                                                                                                       
    m_b_photon_etcone20.push_back (photon->auxdata<float> ("etcone20") * 1e-3);
    m_b_photon_etcone30.push_back (photon->auxdata<float> ("etcone30") * 1e-3);
    m_b_photon_etcone40.push_back (photon->auxdata<float> ("etcone40") * 1e-3);
    m_b_photon_topoetcone20.push_back (photon->auxdata<float> ("topoetcone20") * 1e-3);
    m_b_photon_topoetcone30.push_back (photon->auxdata<float> ("topoetcone30") * 1e-3);
    m_b_photon_topoetcone40.push_back (photon->auxdata<float> ("topoetcone40") * 1e-3);

    //Shower Shapes                                                                                                                                                                    
    m_b_photon_Rhad1.push_back (photon->auxdata<float> ("Rhad1"));
    m_b_photon_Rhad.push_back (photon->auxdata<float> ("Rhad"));
    m_b_photon_e277.push_back (photon->auxdata<float> ("e277"));
    m_b_photon_Reta.push_back (photon->auxdata<float> ("Reta"));
    m_b_photon_Rphi.push_back (photon->auxdata<float> ("Rphi"));
    m_b_photon_weta1.push_back (photon->auxdata<float> ("weta1"));
    m_b_photon_weta2.push_back (photon->auxdata<float> ("weta2"));
    m_b_photon_wtots1.push_back (photon->auxdata<float> ("wtots1"));
    m_b_photon_f1.push_back (photon->auxdata<float> ("f1"));
    m_b_photon_f3.push_back (photon->auxdata<float> ("f3"));
    m_b_photon_fracs1.push_back (photon->auxdata<float> ("fracs1"));
    m_b_photon_DeltaE.push_back (photon->auxdata<float> ("DeltaE"));
    m_b_photon_Eratio.push_back (photon->auxdata<float> ("Eratio"));


    m_b_photon_n++;

  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_eb_L1TE50");
    HLT_noalg_eb_L1TE50 = chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_eb_L1EM12");
    HLT_noalg_eb_L1EM12 = chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_eb_L1EM14");
    HLT_noalg_eb_L1EM14= chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_eb_L1EM16");
    HLT_noalg_eb_L1EM16=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_eb_L1EM22");
    HLT_noalg_eb_L1EM22=chainGroup->isPassed();
  }

  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_mb_L1TE50");
    HLT_noalg_mb_L1TE50 = chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_eb_L1TE12000");
    HLT_noalg_eb_L1TE12000 = chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_cc_L1TE600_0ETA49");
    HLT_noalg_cc_L1TE600_0ETA49 = chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_eb_L1ZDC_A_C_VTE50");
    HLT_noalg_eb_L1ZDC_A_C_VTE50 = chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_eb_L1MU4");
    HLT_noalg_eb_L1MU4 =chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g15_loose_L1EM7");
    HLT_g15_loose_L1EM7=chainGroup->isPassed();
  }

  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g20_loose_L1EM15");
    HLT_g20_loose_L1EM15=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g25_loose_L1EM15");
    HLT_g25_loose_L1EM15=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g30_loose_L1EM15");
    HLT_g30_loose_L1EM15=chainGroup->isPassed();
  }
  bool check_trig = false;
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g35_loose_L1EM15");
    HLT_g35_loose_L1EM15=chainGroup->isPassed();
    check_trig = HLT_g35_loose_L1EM15;
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g30_loose_ion");
    HLT_g30_loose_ion=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g50_loose_ion");
    HLT_g50_loose_ion=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g15_loose_ion");
    HLT_g15_loose_ion=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g20_loose");
    HLT_g20_loose=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g20_loose_ion");
    HLT_g20_loose_ion=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g13_etcut_ion");
    HLT_g13_etcut_ion=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g18_etcut_ion");
    HLT_g18_etcut_ion=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g18_etcut");
    HLT_g18_etcut=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g28_etcut_ion");
    HLT_g28_etcut_ion=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_g50_loose_ion");
    HLT_g50_loose_ion=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_L1EM10");
    HLT_noalg_L1EM10=chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_noalg_L1EM12");
    HLT_noalg_L1EM12=chainGroup->isPassed();
  }

  if(m_b_photon_n > 0) {tree ("analysis")->Fill ();}

  return StatusCode::SUCCESS;
}



StatusCode MyTriggerAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  //// Delete jet calibration tools                                                                                     
  /**
  if(m_Akt2HI_EM_EtaJES_CalibTool){
    delete m_Akt2HI_EM_EtaJES_CalibTool;
    m_Akt2HI_EM_EtaJES_CalibTool = nullptr;
  }

  if(m_Akt4HI_EM_EtaJES_CalibTool){
    delete m_Akt4HI_EM_EtaJES_CalibTool;
    m_Akt4HI_EM_EtaJES_CalibTool = nullptr;
  }

  if(m_Akt3HI_EM_EtaJES_CalibTool){
    delete m_Akt3HI_EM_EtaJES_CalibTool;
    m_Akt3HI_EM_EtaJES_CalibTool = nullptr;
  }

  */

  return StatusCode::SUCCESS;
}
