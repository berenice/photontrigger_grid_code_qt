#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
//#include <PhotonJetAnalysis/TreeMaker.h>                                                                                                                                                                
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>

void ATestSubmit (const std::string& submitDir)
{
  // Set up the job for xAOD access:                                                                                                                                                                      
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use                                                                                                                                        
  SH::SampleHandler sh;

  // scan for datasets in the given directory                                                                                                                
  // this works if you are on lxplus, otherwise you'd want to copy over files                                                                              
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory                                                                                        
  // containing all the files, not the subdirectories.                                                                                                       
  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:                                                       
  //const char* inputFilePath = gSystem->ExpandPathName ("$ALRB_TutorialData");                                                                              
  //SH::ScanDir().filePattern("AOD.11182705._000001.pool.root.1").scan(sh,inputFilePath);                                                                    

  SH::scanRucio (sh, "data18_hi.00367273.physics_EnhancedBias.merge.AOD.r11807_r11808_p3516_tid20507344_00/"); //Pb+Pb 2018 (File #1)
  //(Don't Use. Older reprocessing) SH::scanRucio (sh, "data18_hi.00367273.physics_EnhancedBias.merge.AOD.r11796_r11797_p3516_tid20475561_00/"); //Pb+Pb 2018 (File #2)
  //SH::scanRucio (sh, "data17_5TeV.periodM.physics_Main.PhysCont.AOD.pro23_v02/"); //pp 2017                                                                 
  //SH::scanRucio (sh, "mc16_5TeV.423102.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP50_70.merge.AOD.e5094_s3238_r10441_r10210"); //MC pp 2016 (50 < pT < 70)      
  //SH::scanRucio (sh, "mc16_5TeV.423103.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP70_140.merge.AOD.e5094_s3238_r10441_r10210"); //MC pp 2016 (70 < pT < 140)    
  //SH::scanRucio (sh, "mc16_5TeV.423104.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP140_280.merge.AOD.e5094_s3238_r10441_r10210"); //MC pp 2016 (140 < pT < 280)   
  //SH::scanRucio (sh, "mc16_5TeV.423105.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP280_500.merge.AOD.e5094_s3238_r10441_r10210"); //MC pp 2016 (280 < pT < 500)                                               



  //SH::scanRucio (sh, "mc16_5TeV.420156.Sherpa_224_NNPDF30NNLO_SinglePhotonPt50_70_EtaFilter.merge.AOD.e6940_s3238_r10441_r10210"); //pp 2016 Sherpa (50 < pT < 70) 
  //SH::scanRucio (sh, "mc16_5TeV.420157.Sherpa_224_NNPDF30NNLO_SinglePhotonPt70_140_EtaFilter.merge.AOD.e6940_s3238_r10441_r10210"); //pp 2016 Sherpa (70 < pT < 140)                                    
  //SH::scanRucio (sh, "mc16_5TeV.420158.Sherpa_224_NNPDF30NNLO_SinglePhotonPt140_280_EtaFilter.merge.AOD.e6940_s3238_r10441_r10210"); //pp 2016 Sherpa (140 < pT < 280)                                  
  //SH::scanRucio (sh, "mc16_5TeV.420159.Sherpa_224_NNPDF30NNLO_SinglePhotonPt280_500_EtaFilter.merge.AOD.e6940_s3238_r10441_r10210"); //pp 2016 Sherpa (280 < pT < 500)                                  

// set the name of the tree in our files                                                                                                                     
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"                                                                                  
  sh.setMetaString ("nc_tree", "CollectionTree");
  // further sample handler configuration may go here                                                                                                       
  sh.load ("output-ANALYSIS");
  // print out the samples we found                                                                                                                          
  sh.print ();
  // this is the basic description of our job                                                                                                          
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job                                                                                                    
  //job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!                          
  job.outputAdd (EL::OutputStream ("ANALYSIS"));
  // add our algorithm to the job                                                                                                                            
  EL::AnaAlgorithmConfig alg;
  alg.setType ("MyTriggerAnalysis");  //-----Photon+-Jet InSitu Study                                                                                        
  // set the name of the algorithm (this is the name use with                                                                                                 
  // messages)                                                                                                                                               
  alg.setName ("AnalysisAlg");
  // later on we'll add some configuration options for our algorithm that go here                                                       
  job.algsAdd (alg);
  // make the driver we want to use:                                                                                                                         
  // this one works by running the algorithm directly:                                                                                                      
  //EL::DirectDriver driver;                                                                                                                                
  EL::PrunDriver driver;
  //driver.options()->setString(EL::Job::optGridNFilesPerJob, std::to_string(10)); //Test Job                                                                
  driver.options()->setString("nc_outputSampleName", "user.berenice.09252020.photonJetData_TriggersAnalysis_PbPb2018.0000002");
  // driver.options()->setString("nc_outputSampleName", "user.berenice.testing00001.%in:name[2]%.%in:name[6]%");                                             
  // we can use other drivers to run things on the Grid, with PROOF, etc.                                                                                     
  driver.options ()->setString (EL::Job::optGridNFilesPerJob, "5");
  // process the job using the driver                                                                                                                
  driver.submitOnly (job, submitDir);


}

