#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "UserAnalysis::TriggerAnalysisLib" for configuration "RelWithDebInfo"
set_property(TARGET UserAnalysis::TriggerAnalysisLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(UserAnalysis::TriggerAnalysisLib PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libTriggerAnalysisLib.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libTriggerAnalysisLib.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS UserAnalysis::TriggerAnalysisLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_UserAnalysis::TriggerAnalysisLib "${_IMPORT_PREFIX}/lib/libTriggerAnalysisLib.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
